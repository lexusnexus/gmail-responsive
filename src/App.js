import React, {useEffect,useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './pages/Login';
import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import EmailList from "./components/EmailList/EmailList";
import Mail from "./components/Mail/Mail";
import {
  BrowserRouter as
  Router,
  Route,
  Routes
} from "react-router-dom";

export default function App() {
    const [windowDimension, setWindowDimension] = useState(null);

    useEffect(() => {
      setWindowDimension(window.innerWidth);
    }, []);

    useEffect(() => {
      function handleResize() {
        setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
    }, []);

    const isMobile = windowDimension <= 768;


  return( 
    <Router>
      {localStorage.getItem('loginData') ==null
        ? (
          <Login />
        ) : (
          <div className="app">
            <Header />
            <div className="app-body">
              {(isMobile===false) 
                ?  <Sidebar />
                : null
              }
              <Routes>
                <Route path="/" element = {<EmailList />} />
                <Route path = "/mail" element = {<Mail />} />
                
              </Routes>
            </div>
          </div>
        )}
    </Router>
  )
}
