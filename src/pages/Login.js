import React,{useState}  from 'react';
import './../App.css';
import GoogleLogin from 'react-google-login';
import gmailLogo from './../images/Gmail-Logo.png';

export default function Login() {
  
  const [loginData, setLoginData] = useState();
  const [token, setToken] = useState(null);
  
  const handleLogin = (data) => {

    setLoginData(data.profileObj.email);
    localStorage.setItem("mailPic", data.profileObj.imageUrl);
    localStorage.setItem('loginData',data.profileObj.email);
    localStorage.setItem("access_token", data.accessToken);
    setToken(data.accessToken);
  }

  const handleFailure = (result) => {
  console.log(result);
  };
  
  const refresh = () => {
    window.location.reload(false);
  }
  
  return (
    ( token !==null || localStorage.getItem('access_token') !==null )
    ? refresh()   
      : <div>
          <div className="App">
            <header className="App-header">
              <img src={gmailLogo} alt="" height="200"/>
              <h1>React Gmail Clone App</h1>
              <div>
                <GoogleLogin
                  clientId={"753474940263-lk4oadr6jv2k528e68uro1sg7p6kcp60.apps.googleusercontent.com"}
                  buttonText="Log in with Google"
                  onSuccess={handleLogin}
                  onFailure={handleFailure}
                  cookiePolicy={'single_host_origin'}
                  scope="email profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid https://mail.google.com/"
                  >
                </GoogleLogin>
                
              </div>
            </header>
          </div>
        </div>
  )
}