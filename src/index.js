import React from 'react';
// import ReactDOM from 'react-dom';
import { createRoot } from 'react-dom/client';

import App from './App';
import { usePromiseTracker,trackPromise } from "react-promise-tracker";
import Loader from "./components/Loader/Loader";

const LoadingIndicator = props => {
  const { promiseInProgress } = usePromiseTracker();
  return promiseInProgress &&
  <div>
    <Loader />
  </div>
  
};


const container = document.getElementById('root');
const root = createRoot(container);
root.render(
 <>
    <App />
    <LoadingIndicator/>
</>
  );

/*
ReactDOM.render(
  <React.StrictMode>
    <App />
    <LoadingIndicator/>
  </React.StrictMode>,
  document.getElementById('root')
  );
*/