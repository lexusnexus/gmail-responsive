import { Button, IconButton } from "@material-ui/core";
import React from "react";
import "./Sidebar.css";
import AddIcon from "@material-ui/icons/Add";
import InboxIcon from "@material-ui/icons/Inbox";
import StarIcon from "@material-ui/icons/Star";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import NearMeIcon from "@material-ui/icons/NearMe";
import NoteIcon from "@material-ui/icons/Note";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PersonIcon from "@material-ui/icons/Person";
import DuoIcon from "@material-ui/icons/Duo";
import PhoneIcon from "@material-ui/icons/Phone";
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import SidebarOption from "./SidebarOption";
import { Link } from "react-router-dom";

function Sidebar() {

  return (
    <div className="sidebar">
      <Button
        className="sidebar-compose"
        startIcon={<CreateOutlinedIcon fontSize="large" />}
      >
        Compose
      </Button>
      <Link to="/" className="sidebar-link">
        <SidebarOption
          Icon={InboxIcon}
          title="Inbox"
          number={100}
          selected={true}
        />
      </Link>

      <SidebarOption Icon={StarIcon} title="Starred" number={23} />
      <SidebarOption Icon={AccessTimeIcon} title="Snoozed" number={6} />
      <SidebarOption Icon={LabelImportantIcon} title="Important" number={11} />
      <SidebarOption Icon={NearMeIcon} title="Sent" number={52} />
      <SidebarOption Icon={NoteIcon} title="Drafts" number={3} />
      <SidebarOption Icon={ExpandMoreIcon} title="More" />

      <div className="sidebar-footer">
        <div className="sidebar-footerIcons">
          <IconButton>
            <PersonIcon />
          </IconButton>
          <IconButton>
            <DuoIcon />
          </IconButton>
          <IconButton>
            <PhoneIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
