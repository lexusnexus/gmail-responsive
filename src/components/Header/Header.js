import React, { useState, useEffect } from "react";
import "./Header.css";
import MenuIcon from "@material-ui/icons/Menu";
import { Avatar, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import AppsIcon from "@material-ui/icons/Apps";
import NotificationsIcon from "@material-ui/icons/Notifications";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import TuneIcon from "@material-ui/icons/Tune";

function Header() {
   const handleLogout = (googleData) => {
      localStorage.removeItem("loginData");
      localStorage.removeItem("access_token");
      localStorage.removeItem("mailPic");
      // setLoginData(null);
      window.location.href = "/";
   };
   let mailPic = localStorage.getItem("mailPic");
   const [windowDimension, setWindowDimension] = useState(null);

   useEffect(() => {
      setWindowDimension(window.innerWidth);
   }, []);

   useEffect(() => {
      function handleResize() {
         setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
   }, []);

   const isMobile = windowDimension <= 767;

   return isMobile === false ? (
      <div className="header">
         <div className="header-left">
            <IconButton>
               <MenuIcon />
            </IconButton>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzRceIIBz4GgeNszaN5SupI6p1SJE_Bzgk3Q&usqp=CAU" alt="gmail logo" />
         </div>
         <div className="header-middle">
            <SearchIcon />
            <input type="text" placeholder="Search all conversations" />
            <IconButton data-toggle="tooltip" data-placement="bottom" title="Show search options">
               <TuneIcon className="header-inputCaret" />
            </IconButton>
         </div>
         <div className="header-right">
            <IconButton data-toggle="tooltip" data-placement="bottom" title="Support">
               <HelpOutlineIcon />
            </IconButton>
            <IconButton data-toggle="tooltip" data-placement="bottom" title="Notifications">
               <NotificationsIcon />
            </IconButton>
            <IconButton data-toggle="tooltip" data-placement="bottom" title="Google Apps">
               <AppsIcon />
            </IconButton>

            <IconButton onClick={handleLogout} data-toggle="tooltip" data-placement="bottom" title="Logout">
               <Avatar src={mailPic} />
            </IconButton>
         </div>
      </div>
   ) : (
      <div className="row headerMobile align-items-baseline justify-content-between">
         <div className="col-auto mr-auto">
            <IconButton>
               <MenuIcon />
            </IconButton>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzRceIIBz4GgeNszaN5SupI6p1SJE_Bzgk3Q&usqp=CAU" alt="gmail logo" height="40px" />
         </div>
         <div className="col-auto">
            <IconButton onClick={handleLogout} data-toggle="tooltip" data-placement="bottom" title="Logout">
               <Avatar src={mailPic} />
            </IconButton>
         </div>
      </div>
   );
}

export default Header;
