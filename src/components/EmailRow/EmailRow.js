import { Checkbox, IconButton } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import "./EmailRow.css";
import StarBorderOutlinedIcon from "@material-ui/icons/StarBorderOutlined";
import LabelImportantOutlinedIcon from "@material-ui/icons/LabelImportantOutlined";
import { useNavigate } from "react-router-dom";

function EmailRow(props) {
   const { inboxProp } = props;
   let time = "";
   let mailDateToday = new Date().toLocaleDateString("en-US", { day: "numeric", month: "short" }).split(" ").reverse().join(" ");

   if (mailDateToday !== inboxProp.mailDateDisplay) {
      time = inboxProp.mailDateDisplay;
   } else {
      time = inboxProp.mailTime;
   }

   let fromName = inboxProp.payload.headers.find((element) => element.name === "From").value.split(" <");
   fromName = fromName[0];
   let bodyPart = "";
   let attArray = [];
   if (inboxProp.payload.mimeType === "multipart/alternative") {
      bodyPart = inboxProp.payload.parts[1].body.data;
   } else if (inboxProp.payload.mimeType === "multipart/mixed") {
      bodyPart = inboxProp.payload.parts[0].body.data;
   } else if (inboxProp.payload.mimeType === "multipart/related") {
      bodyPart = inboxProp.payload.parts[0].parts[1].body.data;
      attArray = inboxProp.attData;
   } else if (inboxProp.payload.mimeType === "text/html") {
      bodyPart = inboxProp.payload.body.data;
   } else {
      bodyPart = inboxProp.payload.body.data;
   }

   const navigate = useNavigate();
   const openMail = () => {
      navigate("/mail", {
         state: {
            id: 1,
            subject: inboxProp.payload.headers.find((element) => element.name === "Subject").value,
            name: inboxProp.payload.headers.find((element) => element.name === "From").value,
            attArray: attArray,
            body: bodyPart,
            time: inboxProp.mailDateTime,
            type: inboxProp.payload.mimeType,
         },
      });
   };
   const [windowDimension, setWindowDimension] = useState(null);

   useEffect(() => {
      setWindowDimension(window.innerWidth);
   }, []);

   useEffect(() => {
      function handleResize() {
         setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
   }, []);

   const isMobile = windowDimension <= 767;
   
   return isMobile === false ? (
      <div onClick={openMail} className="emailRow">
         <div className="emailRow-options">
            <Checkbox />
            <IconButton>
               <StarBorderOutlinedIcon />
            </IconButton>
            <IconButton>
               <LabelImportantOutlinedIcon />
            </IconButton>
         </div>
         <h3 className="emailRow-title">{fromName}</h3>
         <div className="emailRow-message">
            <h4>
               {inboxProp.payload.headers.find((element) => element.name === "Subject").value} <span className="emailRow-description"> - {inboxProp.snippet.replace(/&#39;/g, "'").replace(/&lt;/g, "<").replace(/&gt;/g, ">")}</span>
            </h4>
         </div>
         <p className="emailRow-time">{time}</p>
      </div>
   ) : (
      <div onClick={openMail} className="row align-items-center mailRowMobile">
         <div className="col-2">
            <Checkbox />
         </div>
         <div className="col-10 py-3 wrapEllipsis senderInfo">
            <h5 className="wrapEllipsis">{fromName}</h5>
            <h6 className="wrapEllipsis">{inboxProp.payload.headers.find((element) => element.name === "Subject").value}</h6>
            <p className="wrapEllipsis">{inboxProp.snippet.replace(/&#39;/g, "'").replace(/&lt;/g, "<").replace(/&gt;/g, ">")}</p>
            <p className="">{time}</p>
         </div>
      </div>
   );
}

export default EmailRow;
